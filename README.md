# Primary Input on LockScreen

## A GNOME Shell 45+ extension that automatically switches the keyboard layout on the lock screen to the primary one.
This is a small, simple extension that solves an annoying issue for users with several keyboard layouts (until it gets solved upstream).

Whenever the lock screen kicks in, this extension checks for the currently selected keyboard layout and switches it to the primary one (the first one in your list of keyboard layouts in Settings).

#### Installation
[<img src="https://user-images.githubusercontent.com/15643750/212080370-77899e64-bae8-43f1-b67a-fc946785c4b3.png" height="100">](https://extensions.gnome.org/extension/4727/primary-input-on-lockscreen/)

Alternatively, use the [Extension Manager](https://github.com/mjakeman/extension-manager) app.

#### Manual installation
1. Download the extension .zip archive from this repo and unzip it
2. Copy the `primary_input_on_lockscreen@sagidayan.com` folder to `~/.local/share/gnome-shell/extensions/`
3. Log out and log back in (on Wayland) or use `Alt+F2`,`r`,`Enter` (on X11)
4. Enable the extension in either `Extensions`, `Extension Manager` or [GNOME Shell Extensions](https://extensions.gnome.org/local/)
